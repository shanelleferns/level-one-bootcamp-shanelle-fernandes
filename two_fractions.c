//WAP to find the sum of two fractions.
#include<stdio.h>

typedef struct fraction{
int n, d;
}fract;

fract input_fract()
{
    fract x;
    printf("Enter the numerator: ");
    scanf("%d", &x.n);
    printf("Enter the denominator: ");
    scanf("%d", &x.d);
    return x;
}
int find_gcd(int a, int b)
{
    if(a==0)
    return b;
    else
    return find_gcd(b%a,a);
}
fract add_fract(fract n1, fract n2)
{
    fract sum;
    sum.n=n1.n*n2.d+n2.n*n1.d;
    sum.d=n1.d*n2.d;
    int hcf=find_gcd(sum.n,sum.d);
    sum.n/=hcf;
    sum.d/=hcf;
    return sum;
}


void output(fract n1, fract n2, fract sum)
{
    if(sum.n==0)
    printf("The sum of %d/%d and %d/%d is 0\n",n1.n,n1.d,n2.n,n2.d);
    else if(sum.n==sum.d)
    printf("The sum of %d/%d and %d/%d is 1\n",n1.n,n1.d,n2.n,n2.d);
    else
    printf("The sum of %d/%d and %d/%d is %d/%d\n",n1.n,n1.d,n2.n,n2.d,sum.n,sum.d);
}

int main()
{
    fract a1, a2, sum;
    printf("The first fraction:\n");
    a1=input_fract();
    printf("The second fraction:\n");
    a2=input_fract();
    sum=add_fract(a1,a2);
    output(a1,a2,sum);
    return 0;
}
