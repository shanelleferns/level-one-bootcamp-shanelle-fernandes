//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>

int no_of_elements()
{
    int n;
    printf("Enter the number of elements: ");
    scanf("%d", &n);
    return n;
}

int input_elements(int n, int a[n])
{
    printf("Enter the elements ot be added: \n ");
    for(int i=0;i<n;i++)
    {
    scanf("%d", &a[i]);
    }
}

int sum_of_elements(int n, int a[n])
{
    int sum=0;
    for(int i=0;i<n;i++)
    {
      sum=sum+a[i];
    }
    return sum;
}

int output(int sum)
{
    printf("The sum of the elements= %d\n", sum);
}

int main()
{
    int n,c;
    n=no_of_elements();
    int a[n];
    input_elements(n,a);
    c=sum_of_elements(n, a);
    output(c);
    return 0;
}
