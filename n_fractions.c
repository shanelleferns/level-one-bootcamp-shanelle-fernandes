#include<stdio.h>

typedef struct Fract{
    int n;
    int d;
}fract;

void input_one_fraction(fract *ptr){
    printf("Enter the numerator\n");
    scanf("%d", &ptr->n);
    printf("Enter the denominator\n");
    scanf("%d", &ptr->d);
}

void input_n_fraction(fract a[ ], int n){
    for(int i=0; i<n;i++){
        input_one_fraction(&a[i]);
    }
}

int gcd(int a, int b){
    if(a == 0){
        return b;
    }else {
        return gcd(b%a, a);
    }
}

int lcm_one_fraction(int a, int b){
    return (a*b)/gcd(a,b);
}

int lcm_n_fraction(fract a[ ], int n){
    int lcm=a[0].d;
    for(int i=1;i<n;i++){
        lcm=lcm_one_fraction(lcm,a[i].d);
    }
    return lcm;
}

fract add_fraction(fract a[ ], int n, int lcm){
    fract sum;
    sum.n=0;
    sum.d=lcm;
    for(int i=0;i<n;i++){
        sum.n+=(lcm/a[i].d)*a[i].n;
    }
    int hcf=gcd(sum.n,sum.d);
    sum.n/=hcf;
    sum.d/=hcf;
    return sum;
}

void output(fract a[ ], int n, fract sum)
{
    printf("The sum of ");
    for(int i=0;i<n;i++){
        if(i==n-1)
            printf("%d/%d=",a[i].n,a[i].d);
        else
            printf("%d/%d+",a[i].n,a[i].d);
    }
    if(sum.n==0)
    printf("0\n");
    else if(sum.n==sum.d)
    printf("1\n");
    else
    printf("%d/%d\n",sum.n,sum.d);
}

int main(){
    int n;
    int lcm;
    fract sum;
    printf("Enter the number of fractions\n");
    scanf("%d",&n);
    fract a[n];
    input_n_fraction(a,n);
    lcm=lcm_n_fraction(a,n);
    sum=add_fraction(a,n,lcm);
    output(a,n,sum);
    return 0;
}

