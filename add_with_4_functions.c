//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>
int input1()
{
    int num;
    printf("Enter number:");
    scanf("%d", &num);
    return num;
}

int input2()
{
    int num;
    printf("Enter number:");
    scanf("%d", &num);
    return num;
}

int sumofnum(int x, int y)
{
    int sum;
    sum= x + y;
    return sum;
}

int output(int sum)
{
    printf("Sum of the numbers are %d", sum);
    
}

int main()
{
    int a, b, c;
    a=input1();
    b=input2();
    c=sumofnum(a,b);
    output(c);
    return 0;
}