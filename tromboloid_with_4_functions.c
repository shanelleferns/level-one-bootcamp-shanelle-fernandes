//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
int input()
{
    int x;
    printf("Enter the side: ");
    scanf("%d", &x);
    return x;
}

int volume(int h, int d, int b)
{
    int v;
    v=(0.33)*((h*d*b)+(d/b));
    return v;
}

int output(int v)
{
    printf("The volume of the tromboloid is: %d \n", v);
}

int main()
{
    int p, q, r, s;
    p=input();
    q=input();
    r=input();
    s=volume(p,q,r);
    output(s);
    return 0;
}