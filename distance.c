//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
int input1()
{
    int a;
    printf("Enter the x co-ordinate: ");
    scanf("%d", &a);
    return a;
}

int input2()
{
    int b;
    printf("Enter the y co-ordinate: ");
    scanf("%d", &b);
    return b;
}

int distance(int x1, int x2, int y1, int y2)
{
    int x, y, d;
    x=x2-x1;
    y=y2-y1;
    d=sqrt((x*x)+(y*y));
    return d;
}

int output(int d)
{
    printf("The distance between the two points= %d \n", d);
}

int main()
{
    int p, q, r, s, c;
    printf("Co-ordinates of first point:\n");
    p=input1();
    q=input2();
    printf("Co-ordinates of second point:\n");
    r=input1();
    s=input2();
    c=distance(p,r,q,s);
    output(c);
    return 0;
}