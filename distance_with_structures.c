//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>

struct point{
int x, y;
}a1, a2;

int input1()
{
    int a;
    printf("Enter the x co-ordinate: ");
    scanf("%d", &a);
    return a;
}

int input2()
{
    int b;
    printf("Enter the y co-ordinate: ");
    scanf("%d", &b);
    return b;
}

int distance(int x1, int x2, int y1, int y2)
{
    int x, y, d;
    x=x2-x1;
    y=y2-y1;
    d=sqrt((x*x)+(y*y));
    return d;
}

int output(int d)
{
    printf("The distance between the two points= %d \n", d);
}

int main()
{
    int c;
    printf("Co-ordinates of first point:\n");
    a1.x=input1();
    a1.y=input2();
    printf("Co-ordinates of second point:\n");
    a2.x=input1();
    a2.y=input2();
    c=distance(a1.x,a2.x,a1.y,a2.y);
    output(c);
    return 0;
}