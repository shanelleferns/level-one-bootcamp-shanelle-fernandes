//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main()
{
    int h, d, b, vol;
    printf("Enter the three sides of the tromboloid: ");
    scanf("%d%d%d", &h, &d, &b);
    vol= (0.33)*((h*d*b)+(d/b));
    printf("The volume of tromboloid is %d", vol);
    return 0;
}


